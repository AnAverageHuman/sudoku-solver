/**
 * Solves a given Sudoku board.
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.FileNotFoundException;

public class ConsoleSolver {
  private final int[][] ORIGINAL = new int[9][9];
  private final int[][] GRID     = new int[9][9];

  public ConsoleSolver(String filename) {
    readFile(filename);
    solve();
    System.out.println(this + "\nNo solution found.");
  }

  @Override
  public String toString() {
    String ret = "";
    for ( int[] i : GRID ) {
      for ( int j : i ) {
        ret += j + " ";
      }
      ret += '\n';
    }
    return ret;
  }

  /**
   * Parse a file for a puzzle.
   * @param   filename  The name of the file to be parsed.
   */
  public void readFile(String filename) {
    try {
      BufferedReader br = new BufferedReader(new FileReader(filename));
      for ( int i = 0; i < 9; i++ ) {
        String[] line = br.readLine().trim().split("\\s+");
        for ( int j = 0; j < 9; j++ ) {
          int k = Integer.parseInt(line[j]);
          GRID[i][j]     = k;
          ORIGINAL[i][j] = k;
        }
      }
    } catch ( FileNotFoundException e ) {
      System.out.println("Couldn't find that file.");
      System.exit(1);
    } catch ( Exception e ) {
      e.printStackTrace();
      System.exit(2);
    }
  }

  /**
   * Checks if placing a number constitues a legal move.
   * @param   x   The x-coordinate of the position.
   * @param   y   The y-coordinate of the position.
   * @param   num The number to put down.
   * @return true if the position is a valid one, false otherwise
   */
  public boolean validPosition(int x, int y, int num) {
    for ( int i : GRID[x] ) {
      if ( i == num ) {
        return false;
      }
    }

    for ( int[] i : GRID ) {
      if ( i[y] == num ) {
        return false;
      }
    }

    final int lower_x = ( x / 3 ) * 3;
    final int lower_y = ( y / 3 ) * 3;
    for ( int _x = 0; _x < 3; _x++ ) {
      for ( int _y = 0; _y < 3; _y++ ) {
        if ( GRID[lower_x + _x][lower_y + _y] == num ) {
          return false;
        }
      }
    }
    return true;
  }

  public boolean solve() {
    for ( int i = 0; i < 9; i++ ) {
      for ( int j = 0; j < 9; j++ ) {
        if ( GRID[i][j] == 0 ) {
          for ( int k = 1; k < 10; k++ ) {
            if ( validPosition(i, j, k) ) {
              GRID[i][j] = k;
              solve();
              GRID[i][j] = 0;
            }
          }

          return false;
        }
      }
    }

    System.out.println(this + "\nSolved!");
    System.exit(0);
    return true;
  }

  public static void main(String[] args) {
    try {
      ConsoleSolver s = new ConsoleSolver(args[0]);
    } catch ( ArrayIndexOutOfBoundsException e ) {
      System.out.println("Need a file name.");
      System.exit(1);
    }
  }
}

